Flip Flop tipo D
================

Descripción en Verilog de un flip flop tipo D con su respectivo banco de
pruebas (testbench).

Resultado
---------

![alt text](ff_d-simulacion.PNG "Simulación Flip Flop tipo D")
