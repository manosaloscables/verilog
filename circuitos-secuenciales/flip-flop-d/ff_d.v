// ******************
// * Flip Flop tipo D 
// ******************
// No tiene reinicio asíncrono

module ff_d
  (
    input wire clk,
    input wire d,
    output reg q
  );

  // Estructura
  always @(posedge clk)
    q <= d;

endmodule
