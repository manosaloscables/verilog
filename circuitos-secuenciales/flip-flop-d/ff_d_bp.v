// *************************************
// Banco de prueba para Flip Flop tipo D
// *************************************

`timescale 1 ns/10 ps

// La directiva `timescale especifica que:
// - la unidad de tiempo de la simulación es de 1 [ns]
// - los pasos de tiempo del simulador son de 10 [ps]

module ff_d_bp();

  // Declaración
  localparam T=20; // Período del reloj
  integer i;  // Contador

  reg  prueba_clk;
  reg  prueba_d;
  wire prueba_q;

  // Instanciación flip flop tido D
  ff_d unidad_ff_d
  (
    .clk(prueba_clk),
    .d(prueba_d),
    .q(prueba_q)
  );

  // Reloj
  // Declarar un reloj de 20 [ns] funcionando indefinidamente.
  always begin
    prueba_clk = 1'b1;
    #(T/2);
    prueba_clk = 1'b0;
    #(T/2);
  end

  // Otros estímulos
  initial begin

    // ==== Estímulo inicial =====
    prueba_d = 1'b0;
    @(negedge prueba_clk);  // Esperar por un tic de reloj

    // ==== Estímulos de prueba =====
    for (i=0; i < 5; i = i+1) begin  // Esperar 5 transiciones
      prueba_d = 1'b0;
      @(negedge prueba_clk);

      prueba_d = 1'b1;
      @(negedge prueba_clk);
    end

    prueba_d = 1'b1;
    repeat(2) @(negedge prueba_clk);  // Esperar dos tics de reloj

    // ==== Parar la simulación ====
    // Volver al modo interactivo de simulación
    $stop;

  end

endmodule
